#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ConferenceAPI.Models;

namespace ConferenceAPI.Controllers {
  [Route("api/[controller]")]
  [ApiController]
  public class ConferencesController : ControllerBase {
    private readonly IConferenceService cs;

    public ConferencesController(IConferenceService conferenceService) {
      cs = conferenceService;
    }

    // GET: api/Conferences
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Conference>>> GetConferences() {
      return Ok(await cs.All());
    }

    // GET: api/Conferences/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Conference>> GetConference(int id) {
      var conference = await cs.Get(id);
      if (conference == null) {
        return NotFound();
      }
      return conference;
    }

    // PUT: api/Conferences/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutConference(int id, Conference conference) {
      if (id != conference.Id) {
        return BadRequest();
      }
      if (await cs.Update(conference)) {
        return NoContent();
      }
      return StatusCode(StatusCodes.Status500InternalServerError);
    }

    // POST: api/Conferences
    [HttpPost]
    public async Task<ActionResult<Conference>> PostConference(Conference conference) {
      await cs.Insert(conference);
      return CreatedAtAction("GetConference", new { id = conference.Id }, conference);
    }

    // DELETE: api/Conferences/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteConference(int? id) {
      if (id == null) return NotFound();

      Conference conference;
      if ((conference = await cs.Get(id)) != null) {
        await cs.Delete(conference.Id);
        return NoContent();
      } else {
        return NotFound();
      }
    }
  }
}
