#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ConferenceAPI.Models;

    public class ConferenceAPIDbContext : DbContext
    {
        public ConferenceAPIDbContext (DbContextOptions<ConferenceAPIDbContext> options)
            : base(options)
        {
        }

        public DbSet<ConferenceAPI.Models.Conference> Conferences { get; set; }
    }
