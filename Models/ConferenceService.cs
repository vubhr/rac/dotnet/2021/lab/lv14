using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace ConferenceAPI.Models;

public class ConferenceService : IConferenceService {
  public ConferenceService(ConferenceAPIDbContext context) {
    Db = context;
  }

  public async Task<IEnumerable> All() {
    return await Db.Conferences.ToListAsync();
  }

  public async Task<Conference?> Get(int? id) {
    return await Db.Conferences.FirstOrDefaultAsync(m => m.Id == id);
  }

  public async Task<int> Insert(Conference? conference) {
    if (conference != null) {
      Db.Add(conference);
      return await Db.SaveChangesAsync();
    }
    return -1;
  }

  public async Task<bool> Update(Conference? conference) {
    if (conference != null) {
      try {
        Db.Update(conference);
        await Db.SaveChangesAsync();
        return true;
      } catch (DbUpdateConcurrencyException) {
        return false;
      }
    }
    return false;
  }

  public async Task<int> Delete(int id) {
    var conference = await Get(id);
    if (conference != null) {
      Db.Conferences.Remove(conference);
      return await Db.SaveChangesAsync();
    }
    return 0;
  }

  public ConferenceAPIDbContext Db { get; }
}