using System.Collections;

namespace ConferenceAPI.Models;

public interface IConferenceService {
  Task<IEnumerable> All();

  Task<Conference?> Get(int? id);

  Task<int> Insert(Conference? conference);

  Task<bool> Update(Conference? conference);

  Task<int> Delete(int id);
}