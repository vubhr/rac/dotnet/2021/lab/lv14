namespace ConferenceAPI.Models;

public class Conference {
  public int Id { get; set; }

  public string? Name { get; set; }

  public string? Location { get; set; }

  public int Attendees { get; set; }
}